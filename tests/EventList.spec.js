/// <reference types="Cypress"/>


import { Events } from "../src/Events"
import { EventList } from "../src/EventList"
import moment from "moment"

describe('class DrawPast EventList', ()=>{

    beforeEach(()=>{
        let test = new EventList
        test.eventList = [
            new Events('testtrue', moment('2019-09-13', true)),
            new Events('testfalse', moment('2019-09-20', false))
        ]
    });

    it('should add a div and put every past event inside', ()=>{

        test.drawPast();
        expect(test.eventList).to.include(EventList.isPast);
    });

});