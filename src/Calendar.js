import moment from "moment";


export class Calendar {
    /**
     * 
     * @param {moment} moment format ISO_8601 : 'YYYY-MM-DD hh:mm'
     */
    constructor(moment = moment()) {
        this.moment = moment;
    }


    /**
     * affiche l'ensemble du mois sous forme d'un calendrier allant du lundi au dimanche
     */
    draw(placeToDraw) {

        placeToDraw.innerHTML = ''

        let div = document.createElement('div');
        div.classList.add('row', 'text-center', 'p-1');

        this.drawWeekDays(div)

        // ajoute les cases vides du début de mois
        this.offsetStart(div)


        // ajoute la case de chacun des jours du mois
        this.daysOfMonth(div)


        // ajoute les cases vides de la fin du mois
        this.offsetEnd(div);

        placeToDraw.appendChild(div);
    }



    daysOfMonth(placeToDraw) {
        for (let i = 0; i < 31; i++) {

            // récupere le 1er jour du mois et lui ajoute 'i' pour connaitre le moment()jour à dessinner
            let dateToDraw = this.moment.startOf('month').add(i, 'days');


            // crée la case et lui applique le style
            let div = document.createElement('div');
            div.classList.add('border', 'border-danger');


            // défini si largeur 1 ou 2 suivant si jour le semaine ou week-end
            if (dateToDraw.day() === 6 || dateToDraw.day() === 0) {
                div.classList.add('col-1');
            } else {
                div.classList.add('col-2');
            }


            // // ajoute un fond coloré si le jour à dessinner est "aujurd'hui"
            if (this.isToday(dateToDraw)) {
                div.classList.add('bg-danger');
            }




            // création et attribution de l'ID grace au moment() du jour à dessiner
            let idToAdd = 'D' + dateToDraw.format('YMMDD');
            div.setAttribute('id', idToAdd);

            // affichage de la date au format "DD mois"
            let p = document.createElement('p');
            p.textContent = dateToDraw.format('D');

            // ajout de la case et du texte au html de la page
            div.appendChild(p);
            placeToDraw.appendChild(div);



            // casse la boucle quand le mois est fini
            if (dateToDraw.date() >= this.moment.endOf('month').date()) {
                break;
            }
        }
    }


    /**
     * décale les jours en fct du jour de la semaine du 1er jour du mois
     */
    offsetStart(placeToDraw) {

        // recupere le dernier jour de la semaine du mois (0 pour dimanche, 6 pour samedi)
        let startDay = this.moment.startOf('month').day()

        // de combien l'offset?
        // (décla à 0 par défaut)
        let offsetW = 0;
        let offsetWE = 0;


        // attribution des bonnes valeurs en fonction
        if (startDay === 2) {
            offsetW = 1;

        } else if (startDay === 3) {
            offsetW = 2;

        } else if (startDay === 4) {
            offsetW = 3;

        } else if (startDay === 5) {
            offsetW = 4;

        } else if (startDay === 6) {
            offsetW = 5;

        } else if (startDay === 0) {
            offsetW = 5;
            offsetWE = 1;
        }


        // création de l'offset
        if (offsetW > 0) {
            for (let i = 0; i < offsetW; i++) {
                this.emptyDiv(placeToDraw)
            }
        }
        if (offsetWE > 0) {
            for (let i = 0; i < offsetWE; i++) {
                this.emptyDivWE(placeToDraw)
            }
        }
    }





    /**
     * fini de remplir la ligne en fct du jour de la semaine du dernier jour du mois
     */
    offsetEnd(placeToDraw) {

        // recupère le dernier jour de la semaine du mois (0 pour dimanche, 6 pour samedi)
        let endDay = this.moment.endOf('month').day()

        // de combien l'offset?
        // décla a l'exterieur à 0 par défaut
        let offsetW = 0;
        let offsetWE = 0;


        // attribution des bonnes valeurs en fonction
        if (endDay === 1) {
            offsetW = 4;
            offsetWE = 2;

        } else if (endDay === 2) {
            offsetW = 3;
            offsetWE = 2;

        } else if (endDay === 3) {
            offsetW = 2;
            offsetWE = 2;

        } else if (endDay === 4) {
            offsetW = 1;
            offsetWE = 2;

        } else if (endDay === 5) {
            offsetWE = 2;

        } else if (endDay === 6) {
            offsetWE = 1;

        }

        // création de l'offset
        if (offsetW > 0) {
            for (let i = 0; i < offsetW; i++) {
                this.emptyDiv(placeToDraw)
            }
        }
        if (offsetWE > 0) {
            for (let i = 0; i < offsetWE; i++) {
                this.emptyDivWE(placeToDraw)
            }
        }
    }






    /**
     * crée une nouvelle div "jour de la semaine" (largeur 2) vide
     */
    emptyDiv(placeToDraw) {
        let div = document.createElement('div');
        div.classList.add('border', 'border-danger', 'col-2', 'bg-secondary', 'emptyDiv')
        placeToDraw.appendChild(div)
    }






    /**
     * crée une nouvelle div "jour de Week-End" (largeur 1) vide
     */
    emptyDivWE(placeToDraw) {
        let div = document.createElement('div');
        div.classList.add('border', 'border-danger', 'col-1', 'bg-secondary', 'emptyDiv');
        placeToDraw.appendChild(div);
    }






    /**
     * verrifie si une date correspond à "aujourd'hui"
     * @param {moment} date 
     */
    isToday(date) {

        if (date.format('YMMDD') === moment().format('YMMDD')) {
            return true;

        } else {
            return false;
        }
    }




    drawWeekDays(placeToDraw) {

        let days = ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche']
        for (let i = 0; i < 7; i++) {
            let div = document.createElement('div');
            div.classList.add('border', 'border-danger', 'bg-dark', 'text-light');

            // défini si largeur 1 ou 2 suivant si jour le semaine ou week-end
            if (i === 5|| i === 6) {
                div.classList.add('col-1');
            } else {
                div.classList.add('col-2');
            }

            let p = document.createElement('p');
            p.textContent = days[i];
            
            div.appendChild(p)
            placeToDraw.appendChild(div)
        }
    }


    showMMMY(placeToDraw){
        placeToDraw.textContent= this.moment.startOf('month').format('MMM Y')
    }
}
