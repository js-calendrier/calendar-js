
import { Form } from "./Form";
import { Events } from "./Events";
import { ReportButtonListen, deleteButtonListen } from "./buttonsListener";







// ajoute place 3 et 4 et current month et list
export function showMeTheCalendar(place1, place2, place3, place4, currentMonth, list) {

    // change le txt
    place1.textContent = "Add";
    place2.textContent = "Past";


    // affichage du titre
    currentMonth.showMMMY(place3);

    // affichage du calendrier
    currentMonth.draw(place4);

    // affichage des evenements à venir 
    list.drawIncoming(place1, place2, place3, place4, currentMonth, list);
    deleteButtonListen(list);
    ReportButtonListen(list);



    // si on clique sur add
    place1.addEventListener('click', () => {
        showMeTheForm(place1, place2, place3, place4, currentMonth, list);
    });

    //  si on clique sur past
    place2.addEventListener('click', () => {
        showMeThePast(place1, place2, place3, place4, currentMonth, list);
    });

}






export function showMeTheForm(place1, place2, place3, place4, currentMonth, list) {

    let form = new Form();

    form.draw(place4);

    // change le txt
    place1.textContent = "Cancel, return to Incomming";
    place2.textContent = "Past";


    // si on clique sur add
    place1.addEventListener('click', () => {
        showMeTheCalendar(place1, place2, place3, place4, currentMonth, list);
    });

    //  si on clique sur past
    place2.addEventListener('click', () => {


        showMeThePast(place1, place2, place3, place4, currentMonth, list);
    });

    // si on clique sur submit
    form.submit.addEventListener('click', () => {
        form.createEvent(list);

        showMeTheCalendar(place1, place2, place3, place4, currentMonth, list);
    });

}






export function showMeThePast(place1, place2, place3, place4, currentMonth, list) {

    list.drawPast(place4);

    // change le txt
    place1.textContent = "Add";
    place2.textContent = "Incomming";

    // si on clique sur add
    place1.addEventListener('click', () => {
        showMeTheForm(place1, place2, place3, place4, currentMonth, list);
    });


    //  si on clique sur past
    place2.addEventListener('click', () => {
        showMeTheCalendar(place1, place2, place3, place4, currentMonth, list);
    });

}

export function showMeReport(place1, place2, place3, place4, currentMonth, list, event) {
    let form = new Form();

    form.drawReport(place4);

    // change le txt
    place1.textContent = "Cancel, return to Incomming";
    place2.textContent = "Past";


    // si on clique sur add
    place2.addEventListener('click', () => {
        showMeTheCalendar(place1, place2, place3, place4, currentMonth, list);
    });

    //  si on clique sur past
    place2.addEventListener('click', () => {


        showMeThePast(place1, place2, place3, place4, currentMonth, list);
    });

    // si on clique sur submit
    form.submit.addEventListener('click', () => {
        form.reportEvent(event);

        showMeTheCalendar(place1, place2, place3, place4, currentMonth, list);
    });

}