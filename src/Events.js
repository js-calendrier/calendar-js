import moment from "moment";

export class Events{
    /**
     * 
     * @param {string} name 
     * @param {string} date format ISO_8601 : 'YYYY-MM-DD hh:mm'
     * 
     */
    constructor(name, date) {
        this.name = name;
        this.date = moment(date);
    }

    
    /**
     * 
     * @param {moment} date format ISO_8601 : 'YYYY-MM-DD hh:mm'
     * 
     */
    report(date) {
        this.date = date;
    }

    
    checkDate() {
        if (this.date > moment()) {
            return false
        }

        if (this.date < moment()) {
            return true
        }
    }

}