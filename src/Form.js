import moment from "moment";
import { EventList } from "./EventList";
import { Events } from "./Events";

export class Form {
    constructor() {
        this.name = null;
        this.day = null;
        this.month = null;
        this.year = null;
        this.hour = null;
        this.minute = null;
        this.submit = null;
    }


    /**
     * 
     * @param {html element} placeToDraw  where you the form will appears
     */
    draw(placeToDraw) {

        placeToDraw.innerHTML = ''


        let div = document.createElement('div');
        div.classList.add('row', 'text-center', 'p-1');

        div.appendChild(this.drawNameArea());

        div.appendChild(this.drawDateArea());

        div.appendChild(this.drawTimeArea());

        div.appendChild(this.drawSubmit())

        placeToDraw.appendChild(div);
    }


    drawReport(placeToDraw) {

        placeToDraw.innerHTML = ''


        let div = document.createElement('div');
        div.classList.add('row', 'text-center', 'p-1');

        div.appendChild(this.drawDateArea());

        div.appendChild(this.drawTimeArea());

        div.appendChild(this.drawSubmit())

        placeToDraw.appendChild(div);

    }





    drawNameArea() {
        let div = document.createElement('div')
        div.classList.add('col-12');

        let label = document.createElement('label');
        label.setAttribute('for', 'formName');
        label.textContent = 'Name';

        let inputName = document.createElement('input');
        this.name = inputName;

        inputName.setAttribute('type', 'text');
        inputName.setAttribute('id', 'formName');

        div.appendChild(label);
        div.appendChild(inputName);
        return div
    }





    drawDateArea() {

        // div

        let div = document.createElement('div')
        div.classList.add('col-12');

        // jour

        let labelDay = document.createElement('label');
        labelDay.setAttribute('for', 'formDay');
        labelDay.textContent = 'Date';

        let selectDay = document.createElement('select');
        this.day = selectDay;

        selectDay.setAttribute('id', 'formDay');

        for (let i = 0; i < 31; i++) {
            let opt = document.createElement('option');
            opt.setAttribute('value', i + 1);
            opt.textContent = i + 1;
            selectDay.appendChild(opt);
        }

        div.appendChild(labelDay);
        div.appendChild(selectDay);





        // mois

        let months = ['Janvier', 'Fevrier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Décembre'];
        let values = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];

        let labelMonth = document.createElement('label');
        labelMonth.setAttribute('for', 'formMonth');
        labelMonth.textContent = '/';

        let selectMonth = document.createElement('select');
        this.month = selectMonth;

        selectMonth.setAttribute('id', 'formMonth');

        for (let i = 0; i < months.length; i++) {
            let opt = document.createElement('option');
            opt.setAttribute('value', values[i]);
            opt.textContent = months[i];
            selectMonth.appendChild(opt);
        }

        div.appendChild(labelMonth);
        div.appendChild(selectMonth);





        // année


        let labelYear = document.createElement('label');
        labelYear.setAttribute('for', 'formYear');
        labelYear.textContent = '/';

        let selectYear = document.createElement('select');
        this.year = selectYear;

        selectYear.setAttribute('id', 'formYear');

        for (let i = 0; i < 11; i++) {
            let currentYear = moment().add(i, 'years').format('Y');
            let opt = document.createElement('option');
            opt.setAttribute('value', currentYear);
            opt.textContent = currentYear;
            selectYear.appendChild(opt);
        }

        div.appendChild(labelYear);
        div.appendChild(selectYear);
        return div;
    }



    drawTimeArea() {
        let div = document.createElement('div');
        div.classList.add('col-12');

        // heure

        let labelHour = document.createElement('label');
        labelHour.setAttribute('for', 'formHour');
        labelHour.textContent = 'Time';

        let selectHour = document.createElement('select');
        this.hour = selectHour;

        selectHour.setAttribute('id', 'formHour');

        for (let i = 0; i < 24; i++) {
            let opt = document.createElement('option');

            if (i < 10) {
                opt.setAttribute('value', '0' + i);
                opt.textContent = '0' + i;
                selectHour.appendChild(opt);

            } else {
                opt.setAttribute('value', i);
                opt.textContent = i;
                selectHour.appendChild(opt);
            }
        }

        div.appendChild(labelHour);
        div.appendChild(selectHour);





        // minutes

        let labelMinute = document.createElement('label');
        labelMinute.setAttribute('for', 'formMinute');
        labelMinute.textContent = '/';

        let selectMinute = document.createElement('select');
        this.minute = selectMinute;

        selectMinute.setAttribute('id', 'formMinute');

        let minutes = 0

        for (let i = 0; i < 12; i++) {

            let opt = document.createElement('option');

            if (minutes < 10) {

                opt.setAttribute('value', '0' + minutes);
                opt.textContent = '0' + minutes;
                selectMinute.appendChild(opt);

            } else {
                opt.setAttribute('value', minutes);
                opt.textContent = minutes;
                selectMinute.appendChild(opt);
            }
            minutes += 5
        }

        div.appendChild(labelMinute);
        div.appendChild(selectMinute);
        return div;
    }

    drawSubmit() {
        let div = document.createElement('div');
        div.classList.add('col-12');

        let inputBtn = document.createElement('input');
        this.submit = inputBtn;

        inputBtn.setAttribute('type', 'button');
        inputBtn.setAttribute('id', 'submit');
        inputBtn.setAttribute('value', 'submit');

        div.appendChild(inputBtn);

        return div
    }







    // |||||||||||||||||||||||||||||||||||||||||||||||||||||
    // |||||||||||||||||||||||||||||||||||||||||||||||||||||
    // |||||||||||||||||||||||||||||||||||||||||||||||||||||
    // |||||||||||||||||||||||||||||||||||||||||||||||||||||
    // |||||||||||||||||||||||||||||||||||||||||||||||||||||
    // |||||||||||||||||||||||||||||||||||||||||||||||||||||
    // |||||||||||||||||||||||||||||||||||||||||||||||||||||


    /**
     * 
     * @param {EventList} eventList 
     */
    createEvent(eventList) {

        let date = this.year.value + '-' + this.month.value + '-' + this.day.value + ' ' + this.hour.value + ':' + this.minute.value;

        eventList.addEvent(this.name.value, date);

    }








    /**
        * 
        * @param {Event} event 
        */
    reportEvent(event) {

        new Events
        let changedDate = this.year.value + '-' + this.month.value + '-' + this.day.value + ' ' + this.hour.value + ':' + this.minute.value;

        event.report(changedDate);

    }
}
