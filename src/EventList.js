import { Events } from "./Events";


export class EventList {
    constructor(name) {
        /**
         * eventList regroupe les new et old event
         *  et attente de draw 
         */

        this.eventList = [];

    }



    /**
     * structure de base pour un draw newEvent
     */
    drawIncoming() {
        for (let i = 0 ; i<this.eventList.length ; i++) {
            console.log("bloup");
            

            if (!this.eventList[i].checkDate()) {
                //creation de l'ID pour ma case
                let dateId = "#D" + this.eventList[i].date.format('YMMDD');
                let caseCalendar = document.querySelector(dateId);
                console.log(caseCalendar);
                let dateIdButton = "D" + this.eventList[i].date.format('YMMDDkkmm');
                let dateHeures = this.eventList[i].date.format('kk:mm');

                //creation de la case .    
                let divEventList = document.createElement('div');
                divEventList.classList.add('EventList');
                divEventList.classList.add('row');
                divEventList.classList.add('border');
                divEventList.classList.add('btn-success');
                divEventList.setAttribute('id', dateIdButton);

                let heures = document.createElement("p");
                heures.classList.add('col-12');
                heures.classList.add("text-center")
                heures.textContent = dateHeures;

                let text = document.createElement("p");
                text.classList.add('col-8');
                text.textContent = this.eventList[i].name;

                //creation du bouton delete.
                let button = document.createElement('button');
                button.classList.add('col-2');
                button.classList.add('btn');
                button.classList.add('btn-danger');
                button.classList.add('border');
                // creation ID bouton different pour le ADDeventListner!
                button.setAttribute('id', dateIdButton + "del");
                button.textContent = "x";

                //creation du bouton modification.
                let buttonEdit = document.createElement('button');
                buttonEdit.classList.add('col-2');
                buttonEdit.classList.add('edit');
                buttonEdit.classList.add('btn');
                buttonEdit.classList.add('btn-warning');
                buttonEdit.classList.add('border');
                // creation ID bouton different pour le ADDeventListner!
                buttonEdit.setAttribute('id', dateIdButton + "edit");

                //mise en place des elements .  
                caseCalendar.appendChild(divEventList);
                divEventList.appendChild(heures);
                divEventList.appendChild(text);
                divEventList.appendChild(buttonEdit);
                divEventList.appendChild(button);
            }
        }
    }



    buttonDelete(eventId) {
        //function qui supprime la selection faite dans la classe DeleteCommande

        function deleteCase(removeId) {

            let deleteCase = document.querySelector(removeId);
            deleteCase.parentNode.removeChild(deleteCase);
        }
        deleteCase("#" + eventId);
    }




   
    drawPast(placeToDraw) {

        placeToDraw.innerHTML = '';

        let divPastEvents = document.createElement('div');
        divPastEvents.classList.add('pastEvent', 'col-12');

        placeToDraw.appendChild(divPastEvents);

        for (let i = 0; i < this.eventList.length; i++) {

            if (this.eventList[i].checkDate()) {
                let text = document.createElement('p');
                text.textContent = '*' + this.eventList[i].name;
                divPastEvents.appendChild(text)
            }
        }
    }







    /**
     * 
     * @param {string} name 
     * @param {moment} date format ISO_8601 : 'YYYY-MM-DD hh:mm'
     */
    addEvent(name, date) {
        this.eventList.push(new Events(name, date));
    }

}