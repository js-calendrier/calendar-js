import { EventList } from "./EventList";




/**
 * écoute le bouton delete
 * @param {EventList} curentEventList 
 */
export function deleteButtonListen(curentEventList) {

    for (const iterator of curentEventList.eventList) {

        if (!iterator.checkDate()) { }
        let dateIdDelete = "#D" + iterator.date.format('YMMDDkkmm') + "del";
        let buttonDelete = document.querySelector(dateIdDelete);

        buttonDelete.addEventListener('click', function () {


            // fonction à apl au click (supression de l'event)
            curentEventList.buttonDelete("D" + iterator.date.format('YMMDDkkmm'));
        });

    }
};




/**
 * écoute le bouton report
 * @param {EventList} curentEventList 
 */
export function reportButtonListen(place1, place2, place3, place4, currentMonth, list) {

    for (const event of list.eventList) {

        if (!event.checkDate()) { }
        let dateIdReport = "#D" + event.date.format('YMMDDkkmm') + "edit";
        let buttonReport = document.querySelector(dateIdReport);

        buttonReport.addEventListener('click', function () {


            //fonction à apl au click (affichage du form report)
            ShowMeReport(place1, place2, place3, place4, currentMonth, list, event);
        });

    }
};