import moment from "moment";
import { EventList } from "./EventList";
import { Calendar } from "./Calendar";
import {showMeTheCalendar} from "./showMe";



// emplacement où on va afficher le calendrier / le formulaire / les évenements passés
let calendar = document.querySelector('#calendar');


// emplacement du titre (mois + année)
let mmmY = document.querySelector('#mmmy');


// recup de la zone à cliquer pour ajouter un evenement
let showForm = document.querySelector('#add');

// recup de la zone à cliquer pour voir le evenements passés
let showPast = document.querySelector('#past');




// |||||||||||||||||||||||||||||||||||||||||||||||||||||
// |||||||||||||||||||||||||||||||||||||||||||||||||||||
// |||||||||||||||||||||||||||||||||||||||||||||||||||||
// |||||||||||||||||||||||||||||||||||||||||||||||||||||
// |||||||||||||||||||||||||||||||||||||||||||||||||||||
// |||||||||||||||||||||||||||||||||||||||||||||||||||||
// |||||||||||||||||||||||||||||||||||||||||||||||||||||


// création du calendrier
let currentMonth = new Calendar(moment());

// création de la liste d'évenements
let list = new EventList();


// ajout de base d'events pr test
list.addEvent('rdv1', "2019-09-14 13:15");
list.addEvent('rdv2', "2019-09-11 13:30");
list.addEvent('rdv3', "2019-09-12 12:40");
list.addEvent('rdv4', "2019-09-15 09:30");
list.addEvent('rdv5', "2019-09-28 13:00");





showMeTheCalendar(showForm, showPast, mmmY, calendar, currentMonth, list);



